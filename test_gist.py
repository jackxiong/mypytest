import pytest

@pytest.mark.slow
def test_our_first_test():
    assert 2==2
def test_one():
    print('\n')
    print('this is my pytest test_one !')

@pytest.mark.skip
def test_should_be_skipped():
    print('skipped it test!')

@pytest.mark.xfail
def test_dont_care_if_fails():
    assert 1==3

class Company:
    def __init__(self,name,stock_symbol):
        self.name=name
        self.stock_symbol=stock_symbol
    def __str__(self):
        return f"{self.name}:{self.stock_symbol}"
    

@pytest.fixture
def company()->Company:
    return Company(name="x",stock_symbol="AETOS")

def test_with_fixture(company:Company):
    print(f"Printing {company} from fixture")

@pytest.mark.parametrize(
    "company_name",["Microsoft","IBM","google"]
)
def test_parametrized(company_name):
    print(f"\n test with {company_name}")
