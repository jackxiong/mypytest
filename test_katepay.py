import pytest
import logging



logger = logging.getLogger("CORONA_LOGS")
@pytest.fixture(scope="class")
def first():
    print("\n获取用户名,scope为class级别只运行一次")
    a = "yoyo"
    return a

class TestClient():
    def test_login_user(self):
        print('\n')
        print('this is test login uer')
        logger.warning("test")
    
    def test_admin_user_login(self,first):
        print('\n')
        print('test admin user login %s'% first)
        assert first == "yoyo"

@pytest.mark.parametrize("n,expected",[(0,0),(3,1),(2,1)])
def test_admin_approve(n,expected):
    try:
        assert n==expected
        print('test admin approve')
    except:
        print('this is not excepted result',n)